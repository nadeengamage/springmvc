<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page isELIgnored="false"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>View Product</title>
</head>
<body>
	<h1>Product Details</h1>
	<c:if test="${ !empty(product)}">
		<h4>Id : ${ product.id }</h4>
		<h4>Name : ${ product.name }</h4>
		<h4>Description : ${ product.description }</h4>
		<h4>Price : ${ product.price }</h4>
	</c:if>
	
	<c:if test="${ empty(product)}">
		<h4>Not Found Products</h4>
	</c:if>
	
	<a href="/curd/home">Back</a>
</body>
</html>