<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page isELIgnored="false"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Home</title>
</head>
<body>
	<h1>Home</h1>
	<a href="add">Add Product</a>
	<br>
	<br>
	<br>
	<br>
	<br>
	<table>
		<tr>
			<th>id</th>
			<th>name</th>
			<th>description</th>
			<th>price</th>
			<th>View</th>
			<th>Edit</th>
			<th>Delete</th>
		</tr>
		<c:forEach var="product" items="${products}">
			<tr>
				<td><c:out value="${product.id}" /></td>
				<td><c:out value="${product.name}" /></td>
				<td><c:out value="${product.description}" /></td>
				<td><c:out value="${product.price}" /></td>
				<td><a href="<c:url value="/view/${product.id}" />">View</a></td>
				<td><a href="<c:url value="/edit/${product.id}" />">Edit</a></td>
				<td><a href="<c:url value="/delete/${product.id}" />">Delete</a></td>
			</tr>
		</c:forEach>
	</table>
</body>
</html>