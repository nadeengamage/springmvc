package com.cms.dao;

import java.util.List;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.cms.model.Product;

@Repository
public class ProductDAOImpl implements ProductDAO {
	
	@Autowired
	private SessionFactory session;

	public void add(Product product) {
		session.getCurrentSession().save(product);
	}

	public Product update(Product product) {
		session.getCurrentSession().update(product);
		return product;
	}

	public void delete(int id) {
		Product product = (Product) session.getCurrentSession().load(Product.class, id);
		
		if (product != null) {
			session.getCurrentSession().delete(product);
		}
	}

	@SuppressWarnings("unchecked")
	public List<Product> getAll() {
		return session.getCurrentSession().createQuery("from Product").list();
	}

	public Product getById(int id) {
		Product product = (Product) session.getCurrentSession().load(Product.class, id);
		System.out.println(product);
		return product;
	}

}
