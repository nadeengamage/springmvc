package com.cms.dao;

import java.util.List;

import com.cms.model.Product;

public interface ProductDAO {
	
	public void add(Product product);
	
	public Product update(Product product);
	
	public void delete(int id);
	
	public List<Product> getAll();
	
	public Product getById(int id);
	
}
