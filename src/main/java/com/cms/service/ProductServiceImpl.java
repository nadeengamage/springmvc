package com.cms.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cms.dao.ProductDAO;
import com.cms.model.Product;

@Service
@Transactional
public class ProductServiceImpl implements ProductService {
	
	@Autowired
	private ProductDAO pdao;

	@Transactional
	public void add(Product product) {
		pdao.add(product);
	}

	@Transactional
	public Product update(Product product) {
		return pdao.update(product);
	}

	@Transactional
	public void delete(int id) {
		pdao.delete(id);
	}

	@Transactional
	public List<Product> getAll() {
		return pdao.getAll();
	}

	@Transactional
	public Product getById(int id) {
		return pdao.getById(id);
	}

}
