package com.cms.service;

import java.util.List;

import com.cms.model.Product;

public interface ProductService {
	
	public void add(Product product);

	public Product update(Product product);

	public void delete(int id);

	public List<Product> getAll();

	public Product getById(int id);
}
