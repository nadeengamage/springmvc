package com.cms.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.cms.model.Product;
import com.cms.service.ProductService;

@Controller
public class HomeController {
	
	@Autowired
	private ProductService productService;
	
	@RequestMapping("/home")
	public ModelAndView home() {
		List<Product> products = productService.getAll();
		return new ModelAndView("home", "products", products);
	}
	
	@RequestMapping("add")
	public ModelAndView add() {
		return new ModelAndView("add");
	}
	
	@RequestMapping(value = "addPost", method = RequestMethod.POST)
	public ModelAndView addProduct(@ModelAttribute Product product) {
		productService.add(product);
		return new ModelAndView("redirect:/home");
	}
	
	@RequestMapping("view/{id}")
	public ModelAndView view(@PathVariable("id") int id) {
		Product product = productService.getById(id);
		return new ModelAndView("view", "product", product);
	}
	
	@RequestMapping("edit/{id}")
	public ModelAndView edit(@PathVariable("id") int id) {
		Product product = productService.getById(id);
		return new ModelAndView("edit", "product", product);
	}
	
	@RequestMapping(value = "editPost", method = RequestMethod.POST)
	public ModelAndView updateDetails(Product product) {
		productService.update(product);
		return new ModelAndView("redirect:/home");
	}
	
	@RequestMapping("delete/{id}")
	public ModelAndView deleteDetails(@PathVariable("id") int id) {
		productService.delete(id);
		return new ModelAndView("redirect:/home");
	}
}
